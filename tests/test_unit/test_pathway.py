"""UNit test for pathway analysis."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import inspect
import os
import warnings

import pytest
from sbtab import SBtab

from equilibrator_api import Q_, Pathway, Reaction, ccache
from helpers import approx_unit


warnings.simplefilter('error', UserWarning)


@pytest.fixture(scope="module")
def toy_pathway() -> Pathway:
    """Create a Pathway object."""
    test_dir = os.path.dirname(
        os.path.abspath(inspect.getfile(inspect.currentframe())))
    sbtab = SBtab.read_csv(os.path.join(test_dir, 'pathway.tsv'), "pathway")
    return Pathway.from_sbtab(sbtab)


def test_shadow_prices(toy_pathway: Pathway):
    """Test the shadow prices of the MDF analysis."""
    warnings.simplefilter('ignore', ResourceWarning)

    net_rxn = toy_pathway.net_reaction

    reference_net_rxn = Reaction.parse_formula(
        ccache.get_compound,
        "2 kegg:C00008 + 2 kegg:C00009 + kegg:C00031 = "
        "2 kegg:C00002 + 2 kegg:C00001 + 2 kegg:C00469 + 2 kegg:C00011")

    print(Reaction._hashable_reactants(net_rxn.sparse_with_phases))
    print(Reaction._hashable_reactants(reference_net_rxn.sparse_with_phases))
    assert net_rxn == reference_net_rxn

    mdf_data = toy_pathway.calc_mdf(stdev_factor=1.0)

    shadow_prices = mdf_data.reaction_df.set_index(
        'reaction_id').shadow_price

    assert shadow_prices['glucokinase'] == pytest.approx(0.0, abs=1e-2)
    assert shadow_prices['ald'] == pytest.approx(0.25, abs=1e-2)
    assert shadow_prices['tim'] == pytest.approx(0.25, abs=1e-2)
    assert shadow_prices['gapdh'] == pytest.approx(0.50, abs=1e-2)

    compound_prices = mdf_data.compound_df.set_index('compound').shadow_price

    assert compound_prices['h2o'] == pytest.approx(0.0, abs=1e-2)
    assert compound_prices['nad'] == pytest.approx(0.5, abs=1e-2)
    assert compound_prices['nadh'] == pytest.approx(-0.5, abs=1e-2)

    # test that the plotting functions run without crashing
    try:
        mdf_data.reaction_plot
        mdf_data.compound_plot
    except Exception:
        pytest.fail("MDF plot function raises exception")


@pytest.mark.parametrize(
    "p_h, ionic_strength, stdev_factor, expected_mdf",
    [
        (Q_("6"), Q_("0.25M"), 1.0, Q_("-2.08 kJ/mol")),
        (Q_("7"), Q_("0.1M"), 1.0, Q_("1.88 kJ/mol")),
        (Q_("8"), Q_("0.1M"), 1.0, Q_("4.77 kJ/mol")),
        (Q_("7"), Q_("0.25M"), 0.0, Q_("2.04 kJ/mol")),
        (Q_("7"), Q_("0.25M"), 1.0, Q_("2.75 kJ/mol")),
        (Q_("7"), Q_("0.25M"), 2.0, Q_("3.46 kJ/mol")),
    ])
def test_mdf(p_h, ionic_strength, stdev_factor, expected_mdf, toy_pathway):
    """Test the MDF analysis result in different conditions."""
    toy_pathway.set_aqueous_params(p_h=p_h, ionic_strength=ionic_strength)
    mdf_data = toy_pathway.calc_mdf(stdev_factor=stdev_factor)
    approx_unit(mdf_data.mdf, expected_mdf, abs=0.1)
