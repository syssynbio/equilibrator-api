"""unit test for balance with oxidation function."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest

from equilibrator_api import Q_, Reaction, ccache


@pytest.fixture(scope="module")
def atp_hydrolysis() -> Reaction:
    """Create a ATP hydrolysis reaction."""
    formula = "KEGG:C00002 + KEGG:C00001 = KEGG:C00008 + KEGG:C00009"
    return Reaction.parse_formula(ccache.get_compound, formula)


def test_serialize(atp_hydrolysis):
    """Test the serialize function of PhasedRection."""
    atp_compound = ccache.get_compound("KEGG:C00002")

    atp_hydrolysis.set_abundance(atp_compound, Q_("10 mM"))

    ser = atp_hydrolysis.serialize()

    assert ser[0]["inchi_key"] == "ZKHQWZAMYRWXGA-KQYNXXCUSA-J"  # ATP
    assert ser[0]["phase"] == "aqueous"
    assert ser[0]["coefficient"] == -1
    assert len(ser[0]["microspecies"]) == 7

    assert ser[1]["inchi"] == "InChI=1S/H2O/h1H2"  # H2O
    assert ser[1]["phase"] == "liquid"
    assert len(ser[1]["microspecies"]) == 1

    assert ser[2]["inchi_key"] == "XTWYTFMLZFPYCI-KQYNXXCUSA-K"  # ADP
    assert len(ser[2]["microspecies"]) == 7
