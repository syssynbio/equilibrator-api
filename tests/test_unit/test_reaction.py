"""unit test for PhaseReaction and ComponentContribution."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import numpy
import pytest

from equilibrator_api import Q_, ComponentContribution, Reaction, ccache
from equilibrator_api.phased_compound import GAS_PHASE_NAME
from helpers import approx_unit


@pytest.fixture(scope="module")
def comp_contribution() -> ComponentContribution:
    """Create a ComponentContribution object."""
    return ComponentContribution(p_h=Q_("7"), ionic_strength=Q_("0.25M"))


@pytest.fixture(scope="module")
def atp_hydrolysis() -> Reaction:
    """Create a ATP hydrolysis reaction."""
    formula = "kegg:C00002 + kegg:C00001 = kegg:C00008 + kegg:C00009"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def missing_h2o() -> Reaction:
    """Create a ATP hydrolysis reaction."""
    formula = "kegg:C00002 = kegg:C00008 + kegg:C00009"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def fermentation_gas() -> Reaction:
    """Create a reaction object of glucose => 2 ethanol + 2 CO2(g)."""
    formula = "kegg:C00031 = 2 kegg:C00469 + 2 kegg:C00011"
    rxn = Reaction.parse_formula(ccache.get_compound, formula)
    rxn.set_phase(ccache.get_compound("KEGG:C00011"), GAS_PHASE_NAME)
    return rxn


def test_water_balancing(comp_contribution, missing_h2o):
    """Test if the reaction can be balanced using H2O."""
    assert not missing_h2o.is_balanced()
    assert missing_h2o.is_balanced(ignore_atoms=("H", "O", "e-"))

    balanced_rxn = missing_h2o.balance_with_compound(
        ccache.get_compound('kegg:C00001'),
        ignore_atoms=("H")
    )
    assert balanced_rxn is not None


def test_atp_hydrolysis_physiological_dg(comp_contribution, atp_hydrolysis):
    """Test the dG adjustments for physiological conditions (with H2O)."""
    warnings.simplefilter('ignore', ResourceWarning)
    assert atp_hydrolysis.is_balanced()
    assert float(atp_hydrolysis.physiological_dg_correction()) == \
        pytest.approx(numpy.log(1e-3), rel=1e-3)

    atp_hydrolysis.set_abundance(ccache.get_compound('kegg:C00002'), Q_("1uM"))
    atp_hydrolysis.set_abundance(ccache.get_compound('kegg:C00008'), Q_("1uM"))
    atp_hydrolysis.set_abundance(ccache.get_compound('kegg:C00009'), Q_("1uM"))

    assert float(atp_hydrolysis.dg_correction()) == \
        pytest.approx(numpy.log(1e-6), rel=1e-3)


def test_fermentation_gas(comp_contribution, fermentation_gas):
    """Test the dG adjustments for physiological conditions (in gas phase)."""
    assert fermentation_gas.is_balanced()
    assert float(fermentation_gas.physiological_dg_correction()) == \
        pytest.approx(numpy.log(1e-9), rel=1e-3)


def test_atp_hydrolysis_dg(comp_contribution, atp_hydrolysis):
    """Test the CC predictions for ATP hydrolysis."""
    warnings.simplefilter('ignore', ResourceWarning)

    atp_hydrolysis.set_abundance(ccache.get_compound('KEGG:C00002'), Q_("1mM"))
    atp_hydrolysis.set_abundance(ccache.get_compound('KEGG:C00008'), Q_("10mM"))
    atp_hydrolysis.set_abundance(ccache.get_compound('KEGG:C00009'), Q_("10mM"))

    standard_dg_prime, dg_uncertainty = comp_contribution.standard_dg_prime(
        atp_hydrolysis)

    approx_unit(standard_dg_prime, Q_("-25.8 kJ/mol"), abs=0.1)
    approx_unit(dg_uncertainty, Q_("0.3 kJ/mol"), abs=0.1)

    dg_prime, _ = comp_contribution.dg_prime(atp_hydrolysis)
    approx_unit(dg_prime, Q_("-31.5 kJ/mol"), abs=0.1)

    physiological_dg_prime, _ = comp_contribution.physiological_dg_prime(
        atp_hydrolysis)
    approx_unit(physiological_dg_prime, Q_("-42.9 kJ/mol"), abs=0.1)


def test_gibbs_energy_pyruvate_decarboxylase(comp_contribution):
    """Test the CC predictions for pyruvate decarboxylase."""
    warnings.simplefilter('ignore', ResourceWarning)

    # pyruvate = acetaldehyde + CO2
    formula = "KEGG:C00022 = KEGG:C00084 + KEGG:C00011"
    reaction = Reaction.parse_formula(ccache.get_compound, formula)

    assert reaction.is_balanced()

    standard_dg_prime, dg_uncertainty = comp_contribution.standard_dg_prime(
        reaction)

    approx_unit(standard_dg_prime, Q_("-18.0 kJ/mol"), abs=0.1)
    approx_unit(dg_uncertainty, Q_("3.3 kJ/mol"), abs=0.1)


def test_reduction_potential(comp_contribution):
    """Test the CC predictions for a redox half-reaction."""
    warnings.simplefilter('ignore', ResourceWarning)

    # oxaloacetate = malate
    formula = "KEGG:C00036 = KEGG:C00149"
    reaction = Reaction.parse_formula(ccache.get_compound, formula)

    assert reaction.check_half_reaction_balancing() == 2
    assert reaction.is_balanced(ignore_atoms=("H", "e-"))

    standard_e_prime, e_uncertainty = comp_contribution.standard_e_prime(
        reaction)

    approx_unit(standard_e_prime, Q_("-177.0 mV"), abs=1.0)
    approx_unit(e_uncertainty, Q_("3.3 mV"), abs=1.0)


def test_unresolved_reactions(comp_contribution):
    """Test the CC predictions for a reaction that cannot be resolved."""
    formula = ("KEGG:C09844 + KEGG:C00003 + KEGG:C00001 => "
               "KEGG:C03092 + KEGG:C00004")

    reaction = Reaction.parse_formula(ccache.get_compound, formula)
    _, dg_uncertainty = comp_contribution.standard_dg_prime(reaction)

    assert float(dg_uncertainty / Q_("kJ/mol")) > 1e4
